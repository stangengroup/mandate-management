package za.co.different.mandate.mandatemanagement.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import za.co.different.mandate.mandatemanagement.entity.Tenant;

@Repository
public interface TenantRepository extends CrudRepository<Tenant, String> {
}
