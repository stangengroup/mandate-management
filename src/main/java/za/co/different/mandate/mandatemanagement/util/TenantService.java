package za.co.different.mandate.mandatemanagement.util;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import za.co.different.mandate.mandatemanagement.repository.TenantRepository;

@Service
public class TenantService {
    private final TenantRepository tenantRepository;

    public TenantService(TenantRepository tenantRepository) {
        this.tenantRepository = tenantRepository;
    }

    @Cacheable("security_groups")
    public Boolean isSecurityGroupEnabled(String id) {
        return tenantRepository.existsById(id);
    }

    // 300000 = 5 minutes
    @Scheduled(fixedDelay = 300000)
    @CacheEvict(value = "security_groups", allEntries = true)
    public void evictAllTenants() {
    }

}
