package za.co.different.mandate.mandatemanagement.sftp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.endpoint.SourcePollingChannelAdapter;
import org.springframework.integration.file.remote.RemoteFileTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.PollableChannel;
import com.jcraft.jsch.ChannelSftp.LsEntry;

/**
 * @author Oleg Zhurakousky
 * @author Gary Russell
 *
 */
public class SftpMandateManResponseFileReceiver {

    private static final Logger logger = LoggerFactory.getLogger(SftpMandateManResponseFileReceiver.class);

    public boolean getFiles(ConfigurableApplicationContext context){
        RemoteFileTemplate<LsEntry> template = null;
        boolean checkAgain = false;
        try {
            PollableChannel localFileChannel = context.getBean(
                    "receiveMandateManagementResponseChannel",
                    PollableChannel.class);
            SourcePollingChannelAdapter adapter = context.getBean(SourcePollingChannelAdapter.class);
            adapter.start();

            Message<?> received;
            received = localFileChannel.receive(1000);

            if (!received.getPayload().toString().isEmpty()) {
                logger.debug("Received file message: " + received);
                checkAgain = true;
            }
        }
        finally {
            context.close();
        }
        return checkAgain;
    }

}
