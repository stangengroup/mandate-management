package za.co.different.mandate.mandatemanagement.util;

public class NoSecurityGroupException extends Exception {
    public NoSecurityGroupException() {
        super("No Security Code");
    }

    public NoSecurityGroupException(String message) {
        super(message);
    }

    public NoSecurityGroupException(String message, Throwable cause) {
        super(message, cause);
    }
}
