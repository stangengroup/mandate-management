package za.co.different.mandate.mandatemanagement.management;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import za.co.different.mandate.mandatemanagement.sftp.SftpMandateManResponseFileReceiver;
import za.co.different.mandate.mandatemanagement.util.NoSecurityGroupException;
import za.co.different.mandate.mandatemanagement.util.SftpRetrieveNotAllowedToRunException;
import za.co.differentlife.types.SecurityGroupEnum;


public class CheckMandateManagementResponseFileBean implements Tasklet{
    private static final Logger logger = LoggerFactory.getLogger(CheckMandateManagementResponseFileBean.class);

    private final SftpMandateManResponseFileReceiver sftpMandateManResponseFileReceiver;
    private String securityGroupId;

    @Value("${sftp.ecentric.mandate.management.remote.directory.download.enabled}")
    private boolean sftpMandateManagementRemoteDirectoryDownloadEnabled;

    ConfigurableApplicationContext context =
            new ClassPathXmlApplicationContext("/config/SftpMandateManagementInboundReceiveResponse-context.xml", this.getClass());

    @Autowired
    public CheckMandateManagementResponseFileBean(SftpMandateManResponseFileReceiver sftpMandateManResponseFileReceiver) {
        this.sftpMandateManResponseFileReceiver = sftpMandateManResponseFileReceiver;
    }

    public void setSecurityGroupId(String securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        if (this.securityGroupId == null) {
            throw new NoSecurityGroupException();
        }
        boolean runMore = false;
        try {
            runMore = checkMandateManagementResponse(SecurityGroupEnum.fromId(this.securityGroupId));
        } catch (SftpRetrieveNotAllowedToRunException e) {
            logger.debug(e.getMessage());
        }
        if (runMore) {
            return RepeatStatus.CONTINUABLE;
        }
        return RepeatStatus.FINISHED;
    }

    private boolean checkMandateManagementResponse(SecurityGroupEnum securityGroup) throws SftpRetrieveNotAllowedToRunException {
        // TODO check for response files and save to file system, then start processor job
        boolean checkAgain;
        if (sftpMandateManagementRemoteDirectoryDownloadEnabled) {
            checkAgain = this.sftpMandateManResponseFileReceiver.getFiles(this.context);
        } else {
            throw new SftpRetrieveNotAllowedToRunException ("Mandate management file receiver not enabled.");
        }
        return checkAgain;
    }
}
