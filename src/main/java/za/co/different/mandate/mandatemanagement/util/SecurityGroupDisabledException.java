package za.co.different.mandate.mandatemanagement.util;

public class SecurityGroupDisabledException extends Exception {
    public SecurityGroupDisabledException(String s) {
        super(s);
    }
}
