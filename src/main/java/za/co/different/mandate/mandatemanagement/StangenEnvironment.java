package za.co.different.mandate.mandatemanagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class StangenEnvironment {
    @Autowired
    Environment environment;

    public void assertNotOnProd() {
        if (isProd()) throw new AssertionError("Not allowed on prod!");
    }

    private boolean isProd() {
        // If any of these three properties have prod-like values we are on prod!
        return environment.getProperty("spring.datasource.url").contains("prod-database") ||
                environment.getProperty("sugar.crm.dlapi.base.url").contains("//crm.differentlife.co.za/") ||
                environment.getProperty("efts.webservice.endpoint").contains("//www.transactionexchange.co.za:444/");
    }
}
