package za.co.different.mandate.mandatemanagement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mangofactory.swagger.configuration.SpringSwaggerConfig;
import com.mangofactory.swagger.models.dto.ApiInfo;
import com.mangofactory.swagger.plugin.EnableSwagger;
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;
import za.co.differentlife.rabbit.QueueGenUtil;
import za.co.differentlife.swagger.SwaggerPluginFactory;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by Ruan Traut on 24/03/2021.
 * Configure swagger
 */
@Configuration
@EnableSwagger
@EnableBatchProcessing
@EnableCaching
public class MandateManagementConfiguration {

    @Value(value = "${rabbitmq.exchange}")
    private String exchangeName;
    private SpringSwaggerConfig springSwaggerConfig;

    @Autowired
    public void setSpringSwaggerConfig(SpringSwaggerConfig springSwaggerConfig) {
        this.springSwaggerConfig = springSwaggerConfig;
    }

    @Bean
    public SwaggerSpringMvcPlugin customImplementation() {
        return SwaggerPluginFactory.buildSwaggerSpringMvcPlugin(".*/*", "1.0-SNAPSHOT", springSwaggerConfig, apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Stangen Mandate Management API",
                "Mandate management integration",
                null,
                null,
                null,
                null
        );
    }

    public static class CustomObjectMapper extends ObjectMapper {
        public CustomObjectMapper() {
            setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
            setSerializationInclusion(JsonInclude.Include.NON_NULL);
            registerModule(new JavaTimeModule());
            configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        }
    }

    @Bean
    public MappingJackson2HttpMessageConverter jacksonMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(new CustomObjectMapper());
        return converter;
    }

    @Bean
    RestOperations restOperations() {
        return new RestTemplate();
    }

    @Bean
    NumberFormat currencyFormatter() {
        return MandateManagementConfiguration.getCurrencyFormatter();
    }

    @Bean
    public TopicExchange batchExchange() {
        return QueueGenUtil.createPersistedExchange(exchangeName);
    }

    public static NumberFormat getCurrencyFormatter() {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("en-ZA"));
        DecimalFormatSymbols randSymbol = ((DecimalFormat) numberFormat).getDecimalFormatSymbols();
        randSymbol.setCurrencySymbol("R");
        randSymbol.setDecimalSeparator('.');
        randSymbol.setMonetaryDecimalSeparator('.');
        randSymbol.setGroupingSeparator(',');
        ((DecimalFormat) numberFormat).setDecimalFormatSymbols(randSymbol);
        return numberFormat;
    }

}
