package za.co.different.mandate.mandatemanagement.job;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import za.co.different.mandate.mandatemanagement.management.CheckMandateManagementResponseFileBean;
//import za.co.different.mandate.mandatemanagement.sftp.SftpClient;
import za.co.different.mandate.mandatemanagement.management.MandateManagementController;
import za.co.different.mandate.mandatemanagement.sftp.SftpMandateManResponseFileReceiver;

public class MandateManagementConfig {

    @Bean
    @StepScope
    public CheckMandateManagementResponseFileBean checkMandateManagementResponseFileTask(
            @Value("#{jobParameters[securityCode]}") String securityCode) {
        CheckMandateManagementResponseFileBean transactionBean = new CheckMandateManagementResponseFileBean(new SftpMandateManResponseFileReceiver());
        transactionBean.setSecurityGroupId(securityCode);
        return transactionBean;
    }

    @Bean
    @StepScope
    public MandateManagementController actionMandateManagementLocalResponseFileTask(
            @Value("#{jobParameters[securityCode]}") String securityCode) {
        MandateManagementController transactionBean = new MandateManagementController();
        transactionBean.setSecurityGroupId(securityCode);
        return transactionBean;
    }

    @Bean
    public TaskletStep checkMandateManagementRemoteResponseFileStep(StepBuilderFactory setBuilderFactory,
                                                      Tasklet checkMandateManagementResponseFileTask,
                                                      StepExecutionListener stepListener) {
        return setBuilderFactory.get("checkMandateManagementRemoteResponseFileStep")
                .listener(stepListener)
                .tasklet(checkMandateManagementResponseFileTask)
                .build();
    }

    @Bean
    public TaskletStep actionMandateManagementLocalResponseFileStep(StepBuilderFactory setBuilderFactory,
                                                                    Tasklet actionMandateManagementLocalResponseFileTask,
                                                                    StepExecutionListener stepListener) {
        return setBuilderFactory.get("checkMandateManagementRemoteResponseFileStep")
                .listener(stepListener)
                .tasklet(actionMandateManagementLocalResponseFileTask)
                .build();
    }

    /**
     * This job will call eccentric to get transaction statuses.
     * @param jobs
     * @param checkMandateManagementRemoteResponseFileStep
     * @param actionMandateManagementLocalResponseFileStep
     * @return
     */
    @Bean
    public Job checkForMandateManagementResponseFilesJob(JobBuilderFactory jobs, TaskletStep checkMandateManagementRemoteResponseFileStep, TaskletStep actionMandateManagementLocalResponseFileStep) {
        return jobs.get("checkForMandateManagementResponseFilesJob")
                .incrementer(new RunIdIncrementer())
                .flow(checkMandateManagementRemoteResponseFileStep)
                .next(actionMandateManagementLocalResponseFileStep)
                .end()
                .build();
    }
}
