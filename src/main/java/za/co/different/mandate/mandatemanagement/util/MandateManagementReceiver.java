package za.co.different.mandate.mandatemanagement.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import za.co.different.mandate.mandatemanagement.vo.StartMandateManagementJobMessage;
import java.io.IOException;
import java.time.ZoneOffset;
import java.util.Date;

/**
 * Receiver used to kick off
 * Created by Ruan Traut on 24/03/2021
 */
public abstract class MandateManagementReceiver {

    @Autowired
    private org.springframework.batch.core.launch.JobLauncher jobLauncher;
    @Autowired
    TenantService tenantService;

    public abstract Logger getLogger();

    protected StartMandateManagementJobMessage deserialize(Message message) throws IOException {
        String strMessage = new String(message.getBody());
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(strMessage, StartMandateManagementJobMessage.class);
    }

    protected void startJob(Job job, Message message) throws IOException, JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, SecurityGroupDisabledException {
        StartMandateManagementJobMessage msg = deserialize(message);
        String securityCode = msg.getSecurityCode();
        if (!tenantService.isSecurityGroupEnabled(securityCode)) {
            throw new SecurityGroupDisabledException("Security Group '" + securityCode + "' is not enabled!");
        }
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("securityCode", securityCode);
        jobParametersBuilder.addDate("date", Date.from(msg.getStartDate().toInstant(ZoneOffset.ofHours(2))));
        jobLauncher.run(job, jobParametersBuilder.toJobParameters());
    }

}
