package za.co.different.mandate.mandatemanagement.receiver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import za.co.different.mandate.mandatemanagement.util.MandateManagementReceiver;
import za.co.different.mandate.mandatemanagement.util.SecurityGroupDisabledException;
import java.io.IOException;

/**
 * Created by Ruan Traut on 24/03/2021.
 * Receiver to check for responses
 */
@Component
public class StartMandateManagementResponseReceiver extends MandateManagementReceiver implements MessageListener {

    private static final Logger logger = LoggerFactory.getLogger(StartMandateManagementResponseReceiver.class);

    private final Job checkForMandateManagementResponseFilesJob;

    @Autowired
    public StartMandateManagementResponseReceiver(@Qualifier("checkForMandateManagementResponseFilesJob") Job checkForMandateManagementResponseFilesJob) {
        this.checkForMandateManagementResponseFilesJob = checkForMandateManagementResponseFilesJob;
    }

    @Override
    public void onMessage(Message message) {
        logger.debug("Received message to start check collection status batch");
        try {
            super.startJob(checkForMandateManagementResponseFilesJob, message);
        } catch (IOException e) {
            logger.error("Failed to read start check collection status batch message from : " + new String(message.getBody()), e);
        } catch (JobInstanceAlreadyCompleteException | JobExecutionAlreadyRunningException | JobParametersInvalidException | JobRestartException | SecurityGroupDisabledException e) {
            logger.error("Failed to start check collection status job. " + e.getMessage(), e);
        }
    }

    @Override
    public Logger getLogger() {
        return logger;
    }
}
