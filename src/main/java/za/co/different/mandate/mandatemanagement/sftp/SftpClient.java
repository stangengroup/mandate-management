//package za.co.different.mandate.mandatemanagement.sftp;
//
//import com.jcraft.jsch.ChannelSftp;
//import org.springframework.core.io.Resource;
//import org.springframework.expression.common.LiteralExpression;
//import org.springframework.integration.annotation.*;
//import org.springframework.integration.core.MessageSource;
//import org.springframework.integration.file.FileNameGenerator;
//import org.springframework.integration.file.filters.AcceptOnceFileListFilter;
//import org.springframework.integration.file.remote.session.CachingSessionFactory;
//import org.springframework.integration.file.remote.session.SessionFactory;
//import org.springframework.integration.sftp.filters.SftpSimplePatternFileListFilter;
//import org.springframework.integration.sftp.inbound.SftpInboundFileSynchronizer;
//import org.springframework.integration.sftp.inbound.SftpInboundFileSynchronizingMessageSource;
//import org.springframework.integration.sftp.outbound.SftpMessageHandler;
//import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
//import org.springframework.messaging.Message;
//import org.springframework.messaging.MessageHandler;
//import org.springframework.messaging.MessagingException;
//import za.co.different.mandate.mandatemanagement.util.SftpRetrieveNotAllowedToRunException;
//
//import java.io.File;
//
//public class SftpClient {
//
//    private String sftpHost;
//    private int sftpPort;
//    private String sftpUser;
//    private Resource sftpPrivateKey;
//    private String sftpPrivateKeyPassphrase;
//    private String sftpPassword;
//    private String sftpRemoteDirectoryUpload;
//    private String sftpRemoteDirectoryDownload;
//    private String sftpLocalDirectoryDownload;
//    private String sftpRemoteDirectoryDownloadFilter;
//    public boolean sftpRemoteDirectoryDownloadEnabled;
//    private String callingClass;
//
//    @ServiceActivator(inputChannel = "toSftpChannel")
//    public MessageHandler mandateManagementUploadHandler() {
//        SftpMessageHandler handler = new SftpMessageHandler(sftpSessionFactory());
//        handler.setRemoteDirectoryExpression(new LiteralExpression(sftpRemoteDirectoryUpload));
//        handler.setFileNameGenerator(new FileNameGenerator() {
//            @Override
//            public String generateFileName(Message<?> message) {
//                if (message.getPayload() instanceof File) {
//                    return ((File) message.getPayload()).getName();
//                } else {
//                    throw new IllegalArgumentException("File expected as payload.");
//                }
//            }
//        });
//        return handler;
//    }
//
//    public SessionFactory<ChannelSftp.LsEntry> sftpSessionFactory() {
//        DefaultSftpSessionFactory factory = new DefaultSftpSessionFactory(true);
//        factory.setHost(sftpHost);
//        factory.setPort(sftpPort);
//        factory.setUser(sftpUser);
//        if (sftpPrivateKey != null) {
//            factory.setPrivateKey(sftpPrivateKey);
//            factory.setPrivateKeyPassphrase(sftpPrivateKeyPassphrase);
//        } else {
//            factory.setPassword(sftpPassword);
//        }
//        factory.setAllowUnknownKeys(false);
//        return new CachingSessionFactory<ChannelSftp.LsEntry>(factory);
//    }
//
//    public SftpInboundFileSynchronizer sftpInboundFileSynchronizer() {
//        SftpInboundFileSynchronizer fileSynchronizer = new SftpInboundFileSynchronizer(sftpSessionFactory());
//        fileSynchronizer.setDeleteRemoteFiles(true);
//        fileSynchronizer.setRemoteDirectory(sftpRemoteDirectoryDownload);
//        fileSynchronizer
//                .setFilter(new SftpSimplePatternFileListFilter(sftpRemoteDirectoryDownloadFilter));
//        return fileSynchronizer;
//    }
//
//    @InboundChannelAdapter(channel = "fromSftpChannel", poller = @Poller(cron = "0 0 0/1 ? * MON-FRI"))
//    // Poller cron config is: second minute hour days-of-month months-of-year days-of-week
//    public MessageSource<File> sftpMandateManagementMessageSource() throws SftpRetrieveNotAllowedToRunException{
//        if (this.sftpRemoteDirectoryDownloadEnabled) {
//            SftpInboundFileSynchronizingMessageSource source = new SftpInboundFileSynchronizingMessageSource(
//                    sftpInboundFileSynchronizer());
//            source.setLocalDirectory(new File(sftpLocalDirectoryDownload));
//            source.setAutoCreateLocalDirectory(true);
//            source.setLocalFilter(new AcceptOnceFileListFilter<File>());
//            return source;
//        }
//        throw new SftpRetrieveNotAllowedToRunException("Polling not enabled for: " + this.getClass().getName());
//    }
//
//    @ServiceActivator(inputChannel = "fromSftpChannel")
//    public MessageHandler resultFileHandler() {
//        return new MessageHandler() {
//            @Override
//            public void handleMessage(Message<?> message) throws MessagingException {
//                System.err.println(message.getPayload());
//            }
//        };
//    }
//
//    @MessagingGateway
//    public interface MandateManagementUploadGateway {
//
//        @Gateway(requestChannel = "toSftpChannel")
//        void upload(File file);
//
//    }
//
//    public String getSftpHost() {
//        return sftpHost;
//    }
//
//    public void setSftpHost(String sftpHost) {
//        this.sftpHost = sftpHost;
//    }
//
//    public int getSftpPort() {
//        return sftpPort;
//    }
//
//    public void setSftpPort(int sftpPort) {
//        this.sftpPort = sftpPort;
//    }
//
//    public String getSftpUser() {
//        return sftpUser;
//    }
//
//    public void setSftpUser(String sftpUser) {
//        this.sftpUser = sftpUser;
//    }
//
//    public Resource getSftpPrivateKey() {
//        return sftpPrivateKey;
//    }
//
//    public void setSftpPrivateKey(Resource sftpPrivateKey) {
//        this.sftpPrivateKey = sftpPrivateKey;
//    }
//
//    public String getSftpPrivateKeyPassphrase() {
//        return sftpPrivateKeyPassphrase;
//    }
//
//    public void setSftpPrivateKeyPassphrase(String sftpPrivateKeyPassphrase) {
//        this.sftpPrivateKeyPassphrase = sftpPrivateKeyPassphrase;
//    }
//
//    public String getSftpPassword() {
//        return sftpPassword;
//    }
//
//    public void setSftpPassword(String sftpPassword) {
//        this.sftpPassword = sftpPassword;
//    }
//
//    public String getSftpRemoteDirectoryUpload() {
//        return sftpRemoteDirectoryUpload;
//    }
//
//    public void setSftpRemoteDirectoryUpload(String sftpRemoteDirectoryUpload) {
//        this.sftpRemoteDirectoryUpload = sftpRemoteDirectoryUpload;
//    }
//
//    public String getSftpRemoteDirectoryDownload() {
//        return sftpRemoteDirectoryDownload;
//    }
//
//    public void setSftpRemoteDirectoryDownload(String sftpRemoteDirectoryDownload) {
//        this.sftpRemoteDirectoryDownload = sftpRemoteDirectoryDownload;
//    }
//
//    public String getSftpLocalDirectoryDownload() {
//        return sftpLocalDirectoryDownload;
//    }
//
//    public void setSftpLocalDirectoryDownload(String sftpLocalDirectoryDownload) {
//        this.sftpLocalDirectoryDownload = sftpLocalDirectoryDownload;
//    }
//
//    public String getSftpRemoteDirectoryDownloadFilter() {
//        return sftpRemoteDirectoryDownloadFilter;
//    }
//
//    public void setSftpRemoteDirectoryDownloadFilter(String sftpRemoteDirectoryDownloadFilter) {
//        this.sftpRemoteDirectoryDownloadFilter = sftpRemoteDirectoryDownloadFilter;
//    }
//
//    public boolean getSftpRemoteDirectoryDownloadEnabled() {
//        return sftpRemoteDirectoryDownloadEnabled;
//    }
//
//    public void setSftpRemoteDirectoryDownloadEnabled(boolean sftpRemoteDirectoryDownloadEnabled) {
//        this.sftpRemoteDirectoryDownloadEnabled = sftpRemoteDirectoryDownloadEnabled;
//    }
//
//    public String getCallingClass() {
//        return callingClass;
//    }
//
//    public void setCallingClass(String callingClass) {
//        this.callingClass = callingClass;
//    }
//}
