package za.co.different.mandate.mandatemanagement.receiver;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import za.co.differentlife.rabbit.QueueGenUtil;
import za.co.differentlife.rabbit.SimpleListenerFactory;

/**
 * Created by Ruan Traut on 24/03/2021.
 * Configure all the listeners for the mandate management service
 */
@Configuration
public class ReceiverConfig {

    private static final int ONE_MIN = 1000 * 60;

    @Value(value = "${rabbitmq.queue.mandate.management.get.response}")
    private String mandateManagementResponseSchedulerQueueName;

    @Value(value = "${rabbitmq.exchange}")
    private String rabbitExchange;

    @Bean
    public Queue startSettlementBatchQueue() {
        return QueueGenUtil.createPersistedQueue(mandateManagementResponseSchedulerQueueName);
    }

    @Bean
    public Binding startSettlementBatchQueueBinding(Queue startSettlementBatchQueue, TopicExchange batchExchange) {
        return BindingBuilder.bind(startSettlementBatchQueue).to(batchExchange).with(mandateManagementResponseSchedulerQueueName);
    }

    @Bean
    public SimpleMessageListenerContainer collectionsContainer(ConnectionFactory connectionFactory, StartMandateManagementResponseReceiver startMandateManagementResponseReceiver) {
        return SimpleListenerFactory.getContainer(connectionFactory, mandateManagementResponseSchedulerQueueName, startMandateManagementResponseReceiver);
    }

}
