package za.co.different.mandate.mandatemanagement.vo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import za.co.differentlife.json.LocalDateTimeSerializer;
import za.co.differentlife.json.LocalDateTimeSugarDeserializer;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by Ruan Traut on 24/03/2021.
 * Message that will be used to kick of mandate management jobs.
 */
public class StartMandateManagementJobMessage implements Serializable {
    @JsonDeserialize(using = LocalDateTimeSugarDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime startDate;
    private String securityCode;

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }
}
