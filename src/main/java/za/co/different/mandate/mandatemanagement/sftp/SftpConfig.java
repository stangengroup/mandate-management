//package za.co.different.mandate.mandatemanagement.sftp;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.io.Resource;
//import za.co.different.mandate.mandatemanagement.management.CheckMandateManagementResponseFileBean;
//import za.co.different.mandate.mandatemanagement.util.SftpRetrieveNotAllowedToRunException;
//
//
//@Configuration
//public class SftpConfig {
//
//    @Value("${sftp.ecentric.mandate.management.host}")
//    private String sftpHost;
//
//    @Value("${sftp.ecentric.mandate.management.port}")
//    private int sftpPort;
//
//    @Value("${sftp.ecentric.mandate.management.user}")
//    private String sftpUser;
//
//    @Value("${sftp.ecentric.mandate.management.privateKey}")
//    private Resource sftpPrivateKey;
//
//    @Value("${sftp.ecentric.mandate.management.privateKeyPassphrase}")
//    private String sftpPrivateKeyPassphrase;
//
//    @Value("${sftp.ecentric.mandate.management.password}")
//    private String sftpPassword;
//
//    @Value("${sftp.ecentric.mandate.management.remote.directory}")
//    private String sftpRemoteDirectoryUpload;
//
//    @Value("${sftp.ecentric.mandate.management.remote.directory.download}")
//    private String sftpRemoteDirectoryDownload;
//
//    @Value("${sftp.ecentric.mandate.management.local.directory.download}")
//    private String sftpLocalDirectoryDownload;
//
//    @Value("${sftp.ecentric.mandate.management.remote.directory.download.filter}")
//    private String sftpRemoteDirectoryDownloadFilter;
//
//    @Value("${sftp.ecentric.mandate.management.remote.directory.download.enabled}")
//    private boolean sftpMandateManagementRemoteDirectoryDownloadEnabled;
//
//    @Bean
//    public SftpClient mandateManagementSftpClient() throws SftpRetrieveNotAllowedToRunException {
//        SftpClient client =  new SftpClient();
//        client.setSftpHost(sftpHost);
//        client.setSftpPort(sftpPort);
//        client.setSftpUser(sftpUser);
//        client.setSftpPassword(sftpPassword);
//        client.setSftpPrivateKey(sftpPrivateKey);
//        client.setSftpPrivateKeyPassphrase(sftpPrivateKeyPassphrase);
//        client.setSftpRemoteDirectoryUpload(sftpRemoteDirectoryUpload);
//        client.setSftpRemoteDirectoryDownload(sftpRemoteDirectoryDownload);
//        client.setSftpRemoteDirectoryDownloadFilter(sftpRemoteDirectoryDownloadFilter);
//        client.setSftpLocalDirectoryDownload(sftpLocalDirectoryDownload);
//        client.setSftpRemoteDirectoryDownloadEnabled(sftpMandateManagementRemoteDirectoryDownloadEnabled);
//        client.setCallingClass(CheckMandateManagementResponseFileBean.class.getName());
//        return client;
//    }
//}
