package za.co.different.mandate.mandatemanagement.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dt_tenant")
public class Tenant {
    @Id
    private String id;

    public Tenant() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
