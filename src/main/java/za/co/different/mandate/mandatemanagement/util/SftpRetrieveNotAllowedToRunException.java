package za.co.different.mandate.mandatemanagement.util;

public class SftpRetrieveNotAllowedToRunException extends Exception {
    public SftpRetrieveNotAllowedToRunException(String s) {
        super(s);
    }
}
