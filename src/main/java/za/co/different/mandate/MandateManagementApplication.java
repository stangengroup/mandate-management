package za.co.different.mandate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;

/**
 * Created by Ruan Traut on 23/10/2021.
 * Mandate Management Service.
 */

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = {
        "za.co.different.jpa",
        "za.co.differentlife.rabbit",
        "za.co.differentlife.dlapi"})
@EntityScan(basePackages = {"za.co.different.jpa.entity", "za.co.differentlife.batch.entity", "za.co.differentlife.batch.view"})
@EnableJpaRepositories(basePackages = {"za.co.different.jpa.repository"})
@IntegrationComponentScan
@EnableIntegration
public class MandateManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(MandateManagementApplication.class, args);
    }
}
