package za.co.different.mandate.rabbit;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ruan Traut on 23/03/2021.
 * Queue generation utils.
 */
public class QueueGenUtil {

    /**
     * Configure a Queue that will move messages to the dlq as per the dlqName after the given delay in milli seconds.
     * Note the binding needs to be configured by the calling class.
     * @param queueName - the delay queue name
     * @param dlqName - The DLQ name or destination queue name
     * @param exchangeName - Exchange to use
     * @param delay - Time out
     * @return The configure Queue instance
     */
    public static Queue createDelayedQueue(String queueName, String dlqName, String exchangeName, int delay) {
        Map<String, Object> arg = new HashMap<>();
        arg.put("x-dead-letter-exchange", exchangeName);
        arg.put("x-dead-letter-routing-key", dlqName);
        arg.put("x-message-ttl", delay);
        return new Queue(queueName,true,false,false, arg);
    }

    public static Queue createPersistedQueue(String queueName) {
        return new Queue(queueName, true, false, false);
    }

    public static TopicExchange createPersistedExchange(String name) {
        return new TopicExchange(name, true, false);
    }

}
